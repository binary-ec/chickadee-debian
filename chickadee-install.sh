#!/bin/bash

sudo apt update

sudo apt install guile-3.0 guile-3.0-dev libfreetype-dev libmpg123-dev libopenal-dev libreadline-dev libvorbis-dev libsdl2-dev libsdl2-ttf-dev libsdl2-mixer-dev libsdl2-image-dev libturbojpeg0-dev

pushd /tmp

wget http://ftp.gnu.org/gnu/guile-opengl/guile-opengl-0.2.0.tar.gz
wget https://files.dthompson.us/releases/chickadee/chickadee-0.10.0.tar.gz
wget https://files.dthompson.us/releases/guile-sdl2/guile-sdl2-0.8.0.tar.gz

tar xf guile-opengl-*.tar.gz
tar xf chickadee-*.tar.gz
tar xf guile-sdl2-*.tar.gz

pushd guile-opengl-0.2.0
./configure --prefix=/usr
make -j4
sudo make install
popd

pushd guile-sdl2-0.8.0
./configure --prefix=/usr
make -j4
sudo make install
popd

pushd chickadee-0.10.0
./configure --prefix=/usr
make -j4
sudo make install
popd

rm -rf guile-opengl-0.2.0
rm -rf guile-sdl2-0.8.0
rm -rf chickadee-0.10.0
popd
